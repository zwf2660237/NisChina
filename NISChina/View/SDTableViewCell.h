//
//  SDTableViewCell.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDTableViewCell : UITableViewCell

@property (nonatomic,strong) NSString * fo;
@property (nonatomic,strong) NSString * by;
@property (nonatomic,strong) NSString * person;

@end
