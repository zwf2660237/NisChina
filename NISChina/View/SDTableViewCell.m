//
//  SDTableViewCell.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "SDTableViewCell.h"

@interface SDTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *forLabel;
@property (weak, nonatomic) IBOutlet UILabel *byLabel;
@property (weak, nonatomic) IBOutlet UILabel *personLabel;


@end

@implementation SDTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if(self.fo) [self setFo:self.fo];
    if(self.person) [self setPerson:self.person];
    if(self.by) [self setBy:self.by];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setFo:(NSString *)fo
{
    _fo = fo;
    self.forLabel.text = fo;
}

- (void)setBy:(NSString *)by
{
    _by = by;
    self.byLabel.text = by;
}

- (void)setPerson:(NSString *)person
{
    _person = person;
    self.personLabel.text = person;
}

@end
