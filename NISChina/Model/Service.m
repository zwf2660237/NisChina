//
//  Service.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/30.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "Service.h"
#import "AFNetworking.h"
#import "User.h"

#define APP_URL @"http://nis.chinaairplus.com/"

@interface Service()



@end

@implementation Service

static Service * nis;


+ (instancetype)nisChina
{
    if(!nis) nis = [[self alloc] init];
    return nis;
}

+ (NSString *)api:(NSString *) uri
{
    return [NSString stringWithFormat:@"%@api/%@",APP_URL,uri];
}

+ (NSString *)url:(NSString *) uri
{
    return [NSString stringWithFormat:@"%@%@",APP_URL,uri];
}

- (void)logInEmail:(NSString *)email password:(NSString *)password  callback:(void(^)(NSDictionary *)) callback
{
    [[AFHTTPSessionManager manager] PUT:[Service api:@"login/login"] parameters:@{@"user_pass":password,@"user_email":email} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        if([[responseObject objectForKey:@"status"] integerValue] > 0)
        {
            NSDictionary * info = [responseObject objectForKey:@"info"];
            User * user = [[User alloc] initWithData:info];
            [User login:user];
        }
        
        callback(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(@{@"status":[NSNumber numberWithBool:NO],@"msg":@"network error"});
    }];
}

- (void)articles:(NSInteger)pageNo callback:(void(^)(NSDictionary *)) callback
{
    [[AFHTTPSessionManager manager] GET:[Service api:@"article/articles"] parameters:@{@"page_no":[NSNumber numberWithInteger:pageNo],@"page_size":@"30"} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        callback(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(@{@"status":[NSNumber numberWithBool:NO],@"msg":@"network error"});
    }];
}

- (void)isLogin:(void(^)(NSDictionary * dict)) callback
{
    if([User current])
    {
        callback(@{@"status":@YES});
        return;
    }
    
    [[AFHTTPSessionManager manager] GET:[Service api:@"login/is_login"] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if([[responseObject objectForKey:@"status"] integerValue] > 0)
        {
            NSDictionary * info = [responseObject objectForKey:@"info"];
            User * user = [[User alloc] initWithData:info];
            [User login:user];
        }
        
        NSLog(@"%@",responseObject);
        callback(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(@{@"status":[NSNumber numberWithBool:NO],@"msg":@"network error"});
    }];
}

@end
