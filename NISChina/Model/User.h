//
//  User.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/31.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * avatorUri;

@property (readonly) NSData * avatorData;

- (instancetype)initWithData:(NSDictionary *)data;

+ (void)clean;
+ (void)login:(User *) user;
+ (User *)current;

@end
