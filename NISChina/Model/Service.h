//
//  Service.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/30.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Service : NSObject

+ (instancetype)nisChina;
+ (NSString *)api:(NSString *) uri;
+ (NSString *)url:(NSString *) uri;

- (void)logInEmail:(NSString *)email password:(NSString *)password  callback:(void(^)(NSDictionary * dict)) callback;

- (void)articles:(NSInteger)pageNo callback:(void(^)(NSDictionary * dict)) callback;

- (void)isLogin:(void(^)(NSDictionary * dict)) callback;

@end
