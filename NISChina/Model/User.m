//
//  User.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/31.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "User.h"

@interface User()

@property (strong,nonatomic) NSMutableDictionary * data;

@end


@implementation User

static User * currentUser;

- (instancetype)initWithData:(NSDictionary *)data
{
    self = [super init];
    
    self.data = [NSMutableDictionary dictionaryWithDictionary:data];
    
    return self;
}

- (NSMutableDictionary *)data
{
    if(!_data)
    {
        _data = [NSMutableDictionary new];
    }
    return _data;
}

- (void)save
{
    NSUserDefaults * df = [NSUserDefaults standardUserDefaults];
    [df setObject:self.data forKey:@"current_user"];
    [df synchronize];
}

+ (void)login:(User *) user
{
    currentUser = user;
    [currentUser save];
}

+ (User *)current
{
    if(!currentUser)
    {
        NSUserDefaults * df = [NSUserDefaults standardUserDefaults];
        NSDictionary * dict = [df objectForKey:@"current_user"];
        if(dict) currentUser = [[User alloc] initWithData:dict];
        
    }
    return currentUser;
}

+ (void)clean
{
    currentUser = nil;
    NSUserDefaults * df = [NSUserDefaults standardUserDefaults];
    [df removeObjectForKey:@"current_user"];
    [df synchronize];
}

- (NSString *)name
{
    return [NSString stringWithFormat:@"%@ %@",[self.data objectForKey:@"user_first_name"],[self.data objectForKey:@"user_last_name"]];
}

- (NSString *)avatorUri
{
    return [self.data objectForKey:@"user_head"];
}

@end
