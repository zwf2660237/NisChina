//
//  main.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/28.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
