//
//  PhotoViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "PhotoViewController.h"

@interface PhotoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *text;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackBtn];
    [self loadEmail:self.email];
    [self loadPhoto:self.name job:self.job room:self.room];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadEmail:(NSString *)email
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"email"] forState:UIControlStateNormal];
    
    
    [btn addTarget:self action:@selector(sendEmail) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.rightBarButtonItem = item;
}

- (void)sendEmail
{
    NSString * url = [NSString stringWithFormat:@"mailto:%@",self.email];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
}

- (void)loadPhoto:(NSString *)name job:(NSString *) job room:(NSString *) room
{
    self.title = name;
    NSString * text = [NSString stringWithFormat:@"%@\nRoom %@\nNIS Wechat Code:NanjingIS\nNIS Wensite:www.nischina.org",job,room];
    self.text.text = text;
    
    NSString * file = [NSString stringWithFormat:@"Assets/photos/%@",name];
    NSData * data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:file ofType:@"png"]];
    [self.photoView setImage:[UIImage imageWithData:data]];
}

@end
