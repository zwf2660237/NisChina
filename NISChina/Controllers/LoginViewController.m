//
//  LoginViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/31.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "LoginViewController.h"
#import "Service.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTextFieldLeftPadding:self.email forWidth:10];
    [self setTextFieldLeftPadding:self.password forWidth:10];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[Service nisChina] isLogin:^(NSDictionary *dict) {
        if([[dict objectForKey:@"status"] integerValue] >0)[self gotoMain];
    }];
}

- (IBAction)login:(UIButton *)sender
{
    [self.view endEditing:YES];
    [sender setEnabled:NO];
    [[Service nisChina] logInEmail:self.email.text password:self.password.text callback:^(NSDictionary *dict) {
        
        if([dict objectForKey:@"status"] > 0)
            [self gotoMain];
        
        [sender setEnabled:YES];
    }];
}

-(void)setTextFieldLeftPadding:(UITextField *)textField forWidth:(CGFloat)leftWidth
{
    CGRect frame = textField.frame;
    frame.size.width = leftWidth;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    textField.leftViewMode = UITextFieldViewModeAlways;
    textField.leftView = leftview;
}

- (void)setShadow:(UITextField *)textField
{
    CALayer *layer = [textField layer];
    
    layer.shadowOffset = CGSizeMake(0, 3);
    
    layer.shadowRadius = 5.0;
    
    layer.shadowColor = [UIColor blackColor].CGColor;
    
    layer.shadowOpacity = 0.8;
    
    [textField.layer addSublayer:layer];
}

- (void)gotoMain
{
    UINavigationController * nav = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"nav"];
    [self presentViewController:nav animated:YES completion:nil];
}
@end
