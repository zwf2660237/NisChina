//
//  WebViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/28.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "WebViewController.h"


@interface WebViewController ()

@property (nonatomic,strong) NSString * pdfName;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webview.frame = self.view.bounds;
    if(self.pdfName) [self loadPdf:self.pdfName];
}

- (WKWebView *)webview
{
    if(!_webview){
        _webview = [[WKWebView alloc] initWithFrame:self.view.bounds];
        _webview.scrollView.bounces = NO;
        [self.view addSubview:_webview];
    }
    return _webview;
}

- (void)loadHtml:(NSString *)name
{
    NSString * pdf = [NSString stringWithFormat:@"Assets/%@",name];
    NSURL * url = [[NSBundle mainBundle] URLForResource:pdf withExtension:nil];
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)loadPdf:(NSString *)name
{
    NSString * pdf = [NSString stringWithFormat:@"Assets/pdf/%@",name];
    NSURL * url = [[NSBundle mainBundle] URLForResource:pdf withExtension:nil];
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)setPdfName:(NSString *)pdfName
{
    _pdfName = pdfName;
}
@end
