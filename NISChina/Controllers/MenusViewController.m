//
//  MenusViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/28.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "MenusViewController.h"
#import "User.h"
#import "Service.h"
#import "AFNetworking.h"

@interface MenusViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *Avator;
@property (weak, nonatomic) IBOutlet UITableView *Menus;

@property (strong,nonatomic) NSArray * rows;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@implementation MenusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.Menus.rowHeight = 56;
    UIView * footer = [[UIView alloc] initWithFrame:CGRectMake(1, 1, 1, 1)];
    footer.backgroundColor = [UIColor clearColor];
    [self.Menus setTableFooterView:footer];
    self.Menus.delegate = self;
    self.Menus.dataSource = self;
    self.nameLabel.text = [User current].name;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString * avator = [Service url:[User current].avatorUri];
    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:avator]]];
    [self.Avator setImage:img forState:UIControlStateNormal];
    self.Avator.clipsToBounds = YES;
    self.Avator.layer.cornerRadius = self.Avator.frame.size.width /2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)choosePhotos:(id)sender {
    
}

- (NSArray *)rows
{
    if(!_rows){
        _rows = @[
            @"News",
            @"Service Directory",
            @"Document",
            @"About Nanjing",
            @"Life in Nanjing",
            @"AirPlus",
            @"Sign Out"
        ];
    }
    return _rows;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * title = [self.rows objectAtIndex:indexPath.row];
    if([title isEqualToString:@"Sign Out"]){
        
    }else{
        [self close:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GOTO" object:@{@"name":title}];
    }
    return NO;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.rows count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"menusCell"];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menusCell"];
    }
    cell.textLabel.text = [self.rows objectAtIndex:indexPath.row];
    //cell.backgroundColor = [UIColor colorWithRed:(245/255.0) green:(245/255.0) blue:(245/255.0) alpha:1];
    return cell;
}

@end
