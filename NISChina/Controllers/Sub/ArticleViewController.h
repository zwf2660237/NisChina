//
//  ArticleViewController.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/30.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "WebViewController.h"

@interface ArticleViewController : WebViewController
- (void)setLanguage:(NSString *)name path:(NSString *)path;
@end
