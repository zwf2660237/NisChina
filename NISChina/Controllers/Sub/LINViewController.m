//
//  LINViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "LINViewController.h"

@interface LINViewController ()

@end

@implementation LINViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showMenuBtn];
    [self loadPdf:@"cityGuide.pdf"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
