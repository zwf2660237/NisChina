//
//  ArticleViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/30.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "ArticleViewController.h"
#import "Service.h"

@interface ArticleViewController ()

@property (nonatomic,strong) NSMutableDictionary * lans;

@end

@implementation ArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showBackBtn];
    if([self.lans count] > 1)[self showLanButton];
    if([self.lans count]){
        [self selectedLanguage:[[self.lans allKeys] firstObject]];
    }
}

- (void)showLanButton
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 40, 40);
    [btn setImage:[UIImage imageNamed:@"lan"] forState:UIControlStateNormal];

    [btn addTarget:self action:@selector(showLans) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
    self.navigationItem.rightBarButtonItem = item;
}

- (NSMutableDictionary *)lans
{
    if(!_lans){
        _lans = [NSMutableDictionary new];
    }
    return _lans;
}

- (void)setLanguage:(NSString *)name path:(NSString *)path
{
    [self.lans setObject:path forKey:name];
}

- (void)showLans
{
    NSArray * lans = [self.lans allKeys];
    UIAlertController * alert = [[UIAlertController alloc] init];
    for (NSString * title in lans) {
        
        [alert addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self selectedLanguage:title];
        }]];
    }
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)selectedLanguage:(NSString *)lan
{
    NSString * path = [self.lans objectForKey:lan];
    NSString * url = [Service url:path];
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

@end
