//
//  NewsViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "NewsViewController.h"
#import "SVPullToRefresh.h"
#import "Service.h"
#import "ArticleViewController.h"

@interface NewsViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;

@property NSMutableArray * list;
@property NSUInteger page;

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showMenuBtn];
    
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.rowHeight = 56;
    self.page = 1;
    
    [self.table addPullToRefreshWithActionHandler:^{
        self.page ++;
        [self loadArticles];
    } position:SVPullToRefreshPositionBottom];
    [self loadArticles];
}

- (void)loadArticles
{
    [[Service nisChina] articles:self.page callback:^(NSDictionary *dict) {
        
        if(self.page == 1)
            self.list = [NSMutableArray new];
        
        [self.table.pullToRefreshView stopAnimating];
        
        if(![dict objectForKey:@"status"]) return;
        
        NSArray * list = [[dict objectForKey:@"info"] objectForKey:@"list"];
        
        if(![list count]) return;
        
        NSMutableArray * arr = [NSMutableArray arrayWithArray:list];
        [self.list addObjectsFromArray:arr];
        [self.table reloadData];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCell"];
    
    if(!cell){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewsCell"];
    }
    
    NSDictionary * data = [self.list objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [data objectForKey:@"article_title"];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * data = [self.list objectAtIndex:indexPath.row];
    ArticleViewController * act = [[ArticleViewController alloc] init];
    act.title = [data objectForKey:@"article_title"];
    NSString * en = [data objectForKey:@"article_pdf_en"];
    if(![en isEqualToString:@""])[act setLanguage:@"English" path:en];
    
    NSString * cn = [data objectForKey:@"article_pdf_cn"];
    if(![cn isEqualToString:@""])[act setLanguage:@"Chinese" path:cn];
    
    NSString * jp = [data objectForKey:@"article_pdf_jp"];
    if(![jp isEqualToString:@""])[act setLanguage:@"Japanese" path:jp];
    
    [self.navigationController pushViewController:act animated:YES];
}

@end
