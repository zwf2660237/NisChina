//
//  SDViewController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "SDViewController.h"
#import "SDTableViewCell.h"
#import "PhotoViewController.h"
#import "Service.h"

@interface SDViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray * list;
@property (nonatomic,strong) NSArray * emails;
@property (weak, nonatomic) IBOutlet UITableView *table;

@end

@implementation SDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showMenuBtn];
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.estimatedRowHeight = 190;
    self.table.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)list
{
    if(!_list){
        NSString * name = @"Assets/json/nis";
        NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@"json"]];
        
        _list = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    }
    return _list;
}

- (NSArray *)emails
{
    if(!_emails){
        NSString * name = @"Assets/json/email";
        NSData *JSONData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@"json"]];
        
        _emails = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingAllowFragments error:nil];
    }
    return _emails;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SDCell"];
    if(!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SDTableViewCell" owner:self options:nil] lastObject];
    }
    
    NSDictionary * data = [self.list objectAtIndex:indexPath.row];
    
    
    cell.fo = [data objectForKey:@"For"];
    cell.by = [data objectForKey:@"By"];
    
    cell.person = [[self namesAtInde:indexPath.row] componentsJoinedByString:@"\n"];
    
    return cell;
}

- (NSArray *)namesAtInde:(NSInteger)index
{
    NSDictionary * data = [self.list objectAtIndex:index];
    NSArray * person = [data objectForKey:@"Person"];
    NSMutableArray * names = [NSMutableArray new];
    for (NSDictionary * p in person) {
        [names addObject:[p objectForKey:@"Name"]];
    }
    return names;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray * names = [self namesAtInde:indexPath.row];
    
    UIAlertController * con = [[UIAlertController alloc] init];
    for (NSString * name in names) {
        [con addAction:[UIAlertAction actionWithTitle:name style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showInfo:name];
        }]];
    }
    [con addAction:[UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:con animated:YES completion:nil];
}

- (void)showInfo:(NSString *)name
{
    NSDictionary * current = nil;
    for (NSDictionary * info in self.emails) {
        NSString * cname = [info objectForKey:@"Name"];
        if([cname isEqual:name]){
            current = info;
            break;
        }
    }
    
    if(!current) return;
    
    NSString * n = [current objectForKey:@"Name"];
    NSString * email = [current objectForKey:@"Email"];
    NSString * room = [current objectForKey:@"Room"];
    NSString * job = [current objectForKey:@"Job"];
    
    PhotoViewController * con = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"Photo"];
    
    con.email = email;
    con.name = n;
    con.room = room;
    con.job = job;
    
    [self.navigationController pushViewController:con  animated:YES];
}

@end
