//
//  WebViewController.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/28.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "MainViewController.h"
#import <WebKit/WebKit.h>

@interface WebViewController : MainViewController

- (void)loadPdf:(NSString *)name;
- (void)setPdfName:(NSString *)pdfName;
- (void)loadHtml:(NSString *)name;

@property (nonatomic,strong) WKWebView * webview;

@end
