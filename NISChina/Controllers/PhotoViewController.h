//
//  PhotoViewController.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "MainViewController.h"

@interface PhotoViewController : MainViewController

@property NSString * email;
@property NSString * name;
@property NSString * job;
@property NSString * room;

@end
