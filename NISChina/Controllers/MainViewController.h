//
//  MainViewController.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

- (void)showMenuBtn;
- (void)showBackBtn;

@end
