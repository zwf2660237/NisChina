//
//  NavController.m
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/29.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import "NavController.h"

@interface NavController ()

@property (strong,nonatomic) NSMutableDictionary * controllers;

@end

@implementation NavController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jump:) name:@"GOTO" object:nil];
}

- (NSMutableDictionary *)controllers
{
    if(!_controllers){
        _controllers = [NSMutableDictionary new];
    }
    return _controllers;
}

- (void)jump:(NSNotification *)notification
{
    NSDictionary * data = [notification object];
    NSString * name = [data objectForKey:@"name"];
    UIViewController * controller = [self.controllers objectForKey:name];
    
    if(!controller){
        UIStoryboard * board = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        controller = [board instantiateViewControllerWithIdentifier:name];
        [self.controllers setObject:controller forKey:name];
        [self pushViewController:controller animated:YES];
    }else{
        if([self.childViewControllers indexOfObject:controller] != NSNotFound)
        {
            [self popToViewController:controller animated:YES];
        }
        else{
            [self pushViewController:controller animated:YES];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
