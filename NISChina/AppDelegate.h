//
//  AppDelegate.h
//  NISChina
//
//  Created by Shixiang Luo on 2018/7/28.
//  Copyright © 2018年 Lian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

